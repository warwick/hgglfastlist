HGGL Fast List Library (Documentation).
Section 1 About the HGGLFastList library.
The HGGLFastList library was designed to address a long standing problem with navigating large lists
on mobile devices.
How it works:
The library implements a dial (preferably transparent) on top of a list. Rotating the dial moves the list up and down. It is possible to manipulate how fast the list moves, depending upon how close the gesture is to the centre of the dial. To vary how fast the list moves in response to the gesture you will need to call: setUseVariablePages(boolean useVariablePages)
History:
The code in this library was based on the HGGLDial, HGDial and HacerGestoV2 gesture libraries. The
HGGLDial and HGDial libraries are custom dial widgets for Android, while the HacerGestoV2 library is a generic Gesture library (also for Android). All these libraries have advanced rotation functions and behaviours.

Before this library existed the best way of navigating large lists was the iOS list control that had an
�out of the box� behaviour, that detected how close a touch was to the bottom or the top of a list
control, which varied the speed of the list movement.
Section 2 Usage:
Implementing this library is done in 4 simple steps.
Step 1. Instantiation Java Usage
hgGlFastList = (HGGLFastList) findViewById(R.id.hgglFastListView);

Step 2. Register the callback:
hgGlFastList.registerCallback(new HGGLFastList.IHGGLFastList() {
	@Override
	public void onDown(HGGLFastListInfo hgglFastListInfo) {}
	@Override
	public void onPointerDown(HGGLFastListInfo hgglFastListInfo) {}
	@Override
	public void onMove(HGGLFastListInfo hgglFastListInfo) {}
	@Override
	public void onPointerUp(HGGLFastListInfo hgglFastListInfo) {}
	@Override
	public void onUp(HGGLFastListInfo hgglFastListInfo) {}
});
Or implement HGGLFastList.IHGGLFastList in your Activity or Fragment and call
hgGlFastList.registerCallback(this)

Step 3. Then call: hgGlFastList.setScrollableView(recyclerView);

Step 4. Setup the XML Layout
<FrameLayout
android:layout_width="match_parent"
	android:layout_height="match_parent"
	android:layout_above="@+id/lLayoutButtonContainer">
	<com.WarwickWestonWright.HGGLFastList.HGGLFastList
		android:id="@+id/hgglFastListView"
		android:layout_width="wrap_content"
		android:layout_height="wrap_content"
		xmlns:app="http://schemas.android.com/apk/res-auto"
		app:hg_drawable="@drawable/hg_fast_list" />
</FrameLayout>

Section 3. HGGLFastListInfo Class Description:
The HGGLFastListInfo is the dynamic return type returned from the libraries callback. This callback hooks into the gesture touch events.
This Class has four fields with classic accessors and mutators:
boolean quickTap;
boolean rotateSnapped;
boolean hasItemIdChanged;
boolean isMaximumReached;
boolean isMinimumReached;
int itemId;
int scrollDirection;
/* Accessors */
boolean getQuickTap()
boolean getRotateSnapped()
boolean getHasItemIdChanged()
boolean getIsMaximumReached()
boolean getIsMinimumReached()
int getItemId()
int getScrollDirection()
/* Mutators */
void setQuickTap(boolean quickTap)
void setRotateSnapped(boolean rotateSnapped)
void setHasItemIdChanged(boolean hasItemIdChanged)
void setIsMaximumReached(boolean isMaximumReached)
void setIsMinimumReached(boolean isMinimumReached)
void setItemId(int itemId)
void setScrollDirection(int scrollDirection)

Section 4 HGGLFastList library public methods and their descriptions.
void registerCallback(HGGLFastList.IHGGLFastList iHGGLFastList)
Function to register libraries main callback all gesture events are returned to this callback.
HGGLFastListInfo getHgGlFastList()
This is a convenience method in case you want to access the dynamic return type outside the
scope of the callback functions.
void setScrollableView(final View scrollableView)
This library at present is only designed to work with RecycleList views. This is a key function that must be set in order for the library to work.
Note: For optimisation and open endedness, there is no code that relies on the ListView type so
refactoring the library to use a different list type is a simple refactoring job.
void setQuickTapTime(long quickTapTime)
Sets the quick tap time sensitivity in milliseconds.
void setAngleSnapBaseOne(final float angleSnapBaseOne, final float angleSnapProximity)
The library can have angle snapping behaviour on the dial that moves the list. The angles are
expressed in ranges of 0 � 1 instead of 0 to 360. For example if the first parameter is 0.125 then this
would cause the rotation to snap ever 45 degrees. If the second parameter was 0.03125 (1 quarter
of 0.125) then the dial would rotate freely for 22.5 degrees before a snap event occurs. Setting the
second parameter to one half of the first parameter would cause no free rotation between snap
events.
void setDialAngle(float fullGestureAngleBaseOne)
Sets the full angle of the dial. Note: this angle relates directly to how far the list has moved.
void setVariableDial(float variableDialInner, float variableDialOuter, float pagesPerDial)
The dial that moves the list up and down can rotate at variable speeds depending on how close the
touch is to the centre of the dial. The first two parameter values dictate how many times the dial
rotates for every rotation of the gesture. The final parameter dictates how many list pages scroll per
full circular gesture. Note: The speed of the dial will not affect the speed of the list unless you call: setUseVariablePages(boolean useVariablePages) with a value of true.
void setUseVariablePages(boolean useVariablePages) IMPORTANT! Key Feature
Calling with a value of true will cause the list to move at a speed relative to the dial rotation speed.
void setSuppressRender(final boolean suppressRender)
When called with a value of false; simply prevents the dial from rotating, but list will still move in the same way.
void performQuickTap()
Simple convenience method to programmatically execute a quick tap event.
long getQuickTapTime()
Simple getter to retrieve the milliseconds of the quick tap tolerance.
float getAngleSnapBaseOne() and float getAngleSnapProximity()
Simple getters to retrieve the values for the angle snapping behaviour.
OnTouchListener getOnTouchListerField()
Convenience method to get a handle on the gesture listener for the library.
void sendTouchEvent(View v, MotionEvent event)
Convenience method to inject live event into the library; causing the library to behave as if the view had been touched directly.
boolean hasAngleSnapped()
Retrieves the value for the angle snapping behaviour; also available from the callback
(HGFastListInfo type).
float getVariableDialInner(), float getVariableDialOuter() and float getItemsPerPage()
Simple getters, to get the inner/outer precision and items per page for variable dial behaviour.
boolean getUseVariablePages()
Retrieves the vale set from setUseVariablePages(boolean useVariablePages)
float getPrecisionRotation()
Retrieves the variable sensitivity for how fast the dial is rotating in relation to how close the
gesture is to the centre of the dial; 1 representing one rotation per circular gesture.
boolean getSuppressRender()
Simple getter to retrieve a state; the state being if the dial will move in response to the gesture.
View getScrollableView()
Simple getter to get the RecyclerListView. This returns a generic View type as an open end to make
is easier to refactor the library to use different ListView types.
float getDialAngle()
Gets the dial angle. Note this value relates direction to how far the list has moved.