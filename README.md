## Welcome to Library HGGLFastList ##
### About This Library: ###
The HGGLFastList library was designed to address a long standing problem with navigating large lists on mobile devices.

**Note:** If you browse this repository you'll find an extended view/canvas version (HGFastList)

**The library uses the Android custom/extended views with canvas and gestures. It supports API 9+.**

**How it works:**

The library implements a dial (preferably transparent) on top of a list. Rotating this dial moves the list up and down. It is possible with this dial to vary how fast the list moves, depending on how close the gesture is to the centre of the dial.

**History:**

The code in this library is based on the HGGLDial, HGDial and HacerGestoV2 gesture libraries. The HGGLDial and HGDial libraries are custom dial widgets for Android, while the HacerGestoV2 library is a generic gesture library (also for Android). All these libraries have advanced rotation functions and behaviors.

Before this library existed the best way of navigating large lists was the iOS list control that had an 'Out-of-the-box' behavior that detected how close a touch was to the bottom or the top of a list control, which in turn, varied the speed of the list movement.

**Notice: HGDialV2 has landed check it out: https://bitbucket.org/warwick/hg_dial_v2**

1. Enhancements include: The way one dial acts upon another (acting dials) is greatly improved, optimised and very intuitive to the developer.
1. The angle snap now functions intuitively with any angle snap angle (the angle no longer has to be evenly divisible by 1).
1. Overall major optimisations.
1. Better separation of concerns.
1. Minimised lines of code.
1. Added new usages (Can now add arrays of dial objects).
1. Added save/restore and flush state objects.
1. Can now interact with multiple dials at the same time.
1. Works fluidly with device rotation.

**Roadmap Note: I will be adding a fling behaviour to this library so that when you fling the dial it will spin causing the list to fling. I’m also considering adding a new variable dial behaviour; causing the acceleration/deceleration to work on a curve. The new variable dial behaviour will only be implemented depending on how useful it turns out to be.**

**Here is a Youtube demonstration: https://youtu.be/LvZN-JC7WwY** (This video is actually for the Canvas version but does exactly the same. This one is actually more responsive)

**Usage:**
Implementing this library is done in 4 simple steps.

**Step 1.** Instantiation Java Usage


```
#!java

HGGLFastList hgGlFastList = (HGGLFastList) findViewById(R.id.hgglFastListView);
```


**Step 2.** Register the callback:


```
#!java

hgGlFastList.registerCallback(new HGGLFastList.IHGGLFastList() {
	@Override
	public void onDown(HGGLFastListInfo hgglFastListInfo) {}
	@Override
	public void onPointerDown(HGGLFastListInfo hgglFastListInfo) {}
	@Override
	public void onMove(HGGLFastListInfo hgglFastListInfo) {}
	@Override
	public void onPointerUp(HGGLFastListInfo hgglFastListInfo) {}
	@Override
	public void onUp(HGGLFastListInfo hgglFastListInfo) {}
});

```

Or implement HGGLFastList.IHGGLFastLis in your Activity or Fragment and call:

```
#!java

hgGlFastList.registerCallback(this);
```


**Step 3.** Then call:


```
#!java

hgGlFastList.setScrollableView(recyclerView);
```


**Step 4.** Setup the XML Layout
XML Layout Usage:


```
#!xml

<FrameLayout
	android:layout_width="match_parent"
	android:layout_height="match_parent"
	android:layout_above="@+id/lLayoutButtonContainer">

	<com.WarwickWestonWright.HGGLFastList.HGGLFastList
		android:id="@+id/hgglFastListView"
		android:layout_width="wrap_content"
		android:layout_height="wrap_content"
		xmlns:app="http://schemas.android.com/apk/res-auto"
		app:hg_drawable="@drawable/hg_fast_list"
		android:layout_gravity="center_horizontal|bottom"/>

</FrameLayout>
```

**Roadmap**

The Library rotation control can have angle snapping behaviour, I may develop a way to simplify co-ordinating the angle snapping with the list movement, ie snap into list positions. I am also planning on adding functions to facilitate single and multi selection lists. At present the demo app does this perfectly. So I will be migrating some of the code from the demo app to the library. Because this library serves a very specific purpose I don't see there being any major updates.

**Known issues:**
At present the only know issues have been resolved.

Please note that this library is only designed to work with the RecyclerView class. I have designed the library so that the functionality and behaviour does not in any way rely on the RecyclerView type, so refactoring the code to work with other list types will be very easy, in fact this was taken into consideration while developing this library. At a later point I will be documenting how to refactor the library to work with other list types.

Any queries please address to the developer at: warwickwestonwright@gmail.com

**LICENSE.**

This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License

In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".

Copyright (c) 2016, Warwick Weston Wright All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1.     Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
1.     Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the

distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.