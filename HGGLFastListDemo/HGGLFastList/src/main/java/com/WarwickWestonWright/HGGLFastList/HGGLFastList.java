/*
License. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2016, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.WarwickWestonWright.HGGLFastList;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import javax.microedition.khronos.opengles.GL10;

public class HGGLFastList extends GLSurfaceView implements HGGLRender.IHGRender {

    private HGGLRender hgRender;
    private static HGGLRender.IHGRender ihgRender;
    private static int imageResourceId;
    private OnTouchListener onTouchListener;
    private static Point viewCenterPoint;
    private static IHGGLFastList iHGGLFastList;
    private static HGGLFastListInfo hgglFastListInfo;
    private static int firstPointerId;
    private static int firstPointerIdx;
    private static float firstTouchX;
    private static float firstTouchY;
    private static int touchPointerCount;
    private static float storedGestureAngleBaseOne;
    private static float currentGestureAngleBaseOne;
    private static float fullGestureAngleBaseOne;
    private static float touchAngle;
    private static float onDownAngleObjectCumulativeBaseOne;
    private static float onUpAngleGestureBaseOne;
    private static float precisionRotation;
    private static float angleSnapBaseOne;
    private static float angleSnapNextBaseOne;
    private static float angleSnapProximity;
    private static boolean angleSnapHasSnapped;
    private static float touchXLocal;
    private static float touchYLocal;
    private static long quickTapTime;
    private static long gestureDownTime;
    private static float distortDialX;
    private static float distortDialY;
    private static boolean suppressRender;
    private static float variableDialInner;
    private static float variableDialOuter;
    private static float imageDiameter;
    private static float viewPortMinimumDimensionOverTwo;
    private static float imageWidth;
    private static float imageHeight;
    private static float viewPortWidth;
    private static float viewPortHeight;
    private static Point centreOffset;
    private static RecyclerView recyclerView;
    private static float recyclerViewHeight;
    private static float verticalRange;
    private static float itemsPerPage;
    private static float pagesPerVerticalRange;
    private static boolean useVariablePages;
    private static float columnCount;
    private static float itemCount;
    private static float pagesPerDial;
    private static int currentItemId;
    private static float maxRotation;
    /* Bottom of block field declarations */

    /* Top of block field declarations */
    public interface IHGGLFastList {

        void onDown(HGGLFastListInfo hgglFastListInfo);
        void onPointerDown(HGGLFastListInfo hgglFastListInfo);
        void onMove(HGGLFastListInfo hgglFastListInfo);
        void onPointerUp(HGGLFastListInfo hgglFastListInfo);
        void onUp(HGGLFastListInfo hgglFastListInfo);

    }//End public interface IHGGLFastList


    public HGGLFastList(Context context, AttributeSet attrs) {
        super(context, attrs);

        setFields();
        final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.HGGLFastList, 0, 0);
        setEGLContextClientVersion(2);
        setZOrderOnTop(true);
        setEGLConfigChooser(8, 8, 8, 8, 16, 0);
        getHolder().setFormat(PixelFormat.RGBA_8888);
        HGGLRender.registerRenderer(ihgRender);

        if(a.hasValue(R.styleable.HGGLFastList_hg_drawable)) {

            imageResourceId = a.getResourceId(a.getIndex(R.styleable.HGGLFastList_hg_drawable), 0);
            a.recycle();
            hgRender = new HGGLRender(getResources(), imageResourceId);
            setRenderer(hgRender);
            setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
            requestRender();

        }

    }//End public HGGLFastList(Context context, AttributeSet attrs)

    @Override
    public void onResume() {
        super.onResume();

        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

    }


    public static void setupMetrics() {

        imageWidth = HGGLRender.getImageHeightAndWidth()[0];
        imageHeight = HGGLRender.getImageHeightAndWidth()[1];
        viewPortWidth = HGGLRender.getViewPortHeightAndWidth()[0];
        viewPortHeight = HGGLRender.getViewPortHeightAndWidth()[1];
        viewCenterPoint = HGGLRender.getViewCenterPoint();
        imageWidth = HGGLRender.getImageHeightAndWidth()[0];
        imageHeight = HGGLRender.getImageHeightAndWidth()[1];
        centreOffset = new Point((int) ((viewPortWidth - imageWidth) / 2f), (int) ((viewPortHeight - imageHeight) / 2f));

        if(imageWidth < imageHeight) {

            imageDiameter = imageWidth;

        }
        else if(imageWidth > imageHeight) {

            imageDiameter = imageHeight;

        }
        else if(imageWidth == imageHeight) {

            imageDiameter = imageWidth;

        }//End if(imageWidth < imageHeight)

        if(viewPortWidth < viewPortHeight) {

            viewPortMinimumDimensionOverTwo = viewPortWidth / 2f;

        }
        else if(viewPortWidth > viewPortHeight) {

            viewPortMinimumDimensionOverTwo = viewPortHeight / 2f;

        }
        else {

            viewPortMinimumDimensionOverTwo = viewPortWidth / 2f;

        }//End if(viewPortWidth < viewPortHeight)

    }//End public static void setupMetrics()


    private void setFields() {

        this.ihgRender = this;
        this.imageResourceId = 0;
        this.viewCenterPoint = null;
        this.iHGGLFastList = null;
        this.firstPointerId = 0;
        this.firstPointerIdx = 0;
        this.firstTouchX = 0f;
        this.firstTouchY = 0f;
        this.storedGestureAngleBaseOne = 0f;
        this.currentGestureAngleBaseOne = 0f;
        this.fullGestureAngleBaseOne = 0f;
        this.touchAngle = 0f;
        this.touchPointerCount = 0;
        this.onDownAngleObjectCumulativeBaseOne = 0f;
        this.onUpAngleGestureBaseOne = 0f;
        this.precisionRotation = 1f;
        this.angleSnapBaseOne = 0f;
        this.angleSnapProximity = 0f;
        this.angleSnapHasSnapped = false;
        this.touchXLocal = 0f;
        this.touchYLocal = 0f;
        this.quickTapTime = 100;
        this.gestureDownTime = 100;
        this.distortDialX = 1f;
        this.distortDialY = 1f;
        this.suppressRender = false;
        this.variableDialInner = 4.5f;
        this.variableDialOuter = 0.8f;
        this.imageDiameter = 0f;
        this.viewPortMinimumDimensionOverTwo = 0f;
        this.imageWidth = 0f;
        this.imageHeight = 0f;
        this.viewPortWidth = 0f;
        this.viewPortHeight = 0f;
        this.angleSnapNextBaseOne = 0f;
        this.recyclerViewHeight = 0f;
        this.verticalRange = 0f;
        this.itemsPerPage = 0f;
        this.pagesPerVerticalRange = 0f;
        this.useVariablePages = true;
        this.columnCount = 1;
        this.itemCount = 0;
        this.pagesPerDial = 1f;
        this.currentItemId = 0;
        this.maxRotation = 0f;
        this.hgglFastListInfo = new HGGLFastListInfo();

    }//End private void setFields()


    public void registerCallback(IHGGLFastList iHGGLFastList) {

        HGGLFastList.iHGGLFastList = iHGGLFastList;

    }//End public void registerCallback(IHGGLFastList iHGGLFastList)


    public HGGLFastListInfo getHgGlFastList() {

        return hgglFastListInfo;

    }


    /* Top of block touch methods */
    private void setDownTouch(final MotionEvent event) {

        try {

            try {

                touchPointerCount = event.getPointerCount();

                if(touchPointerCount == 1) {

                    firstPointerId = event.getPointerId(0);
                    firstPointerIdx = event.findPointerIndex(firstPointerId);
                    firstTouchX = event.getX(firstPointerIdx);
                    firstTouchY = event.getY(firstPointerIdx);

                }

            }
            catch(IndexOutOfBoundsException e) {

                return;

            }

        }
        catch(IllegalArgumentException e) {

            return;

        }

    }//End private void setDownTouch(final MotionEvent event)


    private void setMoveTouch(final MotionEvent event) {

        try {

            try {

                touchPointerCount = event.getPointerCount();

                if(touchPointerCount < 2) {

                    firstPointerId = event.getPointerId(0);
                    firstPointerIdx = event.findPointerIndex(firstPointerId);
                    firstTouchX = event.getX(firstPointerIdx);
                    firstTouchY = event.getY(firstPointerIdx);

                }

            }
            catch(IndexOutOfBoundsException e) {

                return;

            }

        }
        catch(IllegalArgumentException e) {

            return;

        }

    }//End private void setMoveTouch(final MotionEvent event)


    private void setUpTouch(final MotionEvent event) {

        try {

            try {

                touchPointerCount = event.getPointerCount();

                if(touchPointerCount == 1) {

                    firstPointerId = event.getPointerId(0);
                    firstPointerIdx = event.findPointerIndex(firstPointerId);
                    firstTouchX = event.getX(firstPointerIdx);
                    firstTouchY = event.getY(firstPointerIdx);

                }

            }
            catch(IndexOutOfBoundsException e) {

                return;

            }

        }
        catch(IllegalArgumentException e) {

            return;

        }

    }//End private void setUpTouch(final MotionEvent event)
    /* Bottom of block touch methods */


    /* Top of block rotate functions */
    private HGGLFastListInfo doDownDial() {

        //Prepare touches for single or dual touch
        if(touchPointerCount == 1) {

            touchXLocal = firstTouchX;
            touchYLocal = firstTouchY;

        }
        else {

            return hgglFastListInfo;

        }

        if(precisionRotation != 0) {

            touchAngle = HGGeometry.getAngleFromPoint(viewCenterPoint, new Point((int) touchXLocal, (int) touchYLocal));
            onDownAngleObjectCumulativeBaseOne = touchAngle - onUpAngleGestureBaseOne;
            setReturnType();

        }

        return hgglFastListInfo;

    }//End private HGResult doDownDial()


    private HGGLFastListInfo doMoveDial() {

        //Prepare touches for single or dual touch
        if(touchPointerCount == 1) {

            touchXLocal = firstTouchX;
            touchYLocal = firstTouchY;

        }
        else {

            return hgglFastListInfo;

        }

        touchAngle = HGGeometry.getAngleFromPoint(viewCenterPoint, new Point((int) touchXLocal, (int) touchYLocal));
        setReturnType();

        return hgglFastListInfo;

    }//End private HGResult doMoveDial()


    private HGGLFastListInfo doUpDial() {

        //Prepare touches for single or dual touch
        if(touchPointerCount == 1) {

            touchXLocal = firstTouchX;
            touchYLocal = firstTouchY;

        }
        else {

            return hgglFastListInfo;

        }

        if(precisionRotation != 0) {

            touchAngle = HGGeometry.getAngleFromPoint(viewCenterPoint, new Point((int) touchXLocal, (int) touchYLocal));
            onUpAngleGestureBaseOne = (1 - (onDownAngleObjectCumulativeBaseOne - touchAngle)) % 1;
            setReturnType();

        }

        return hgglFastListInfo;

    }//End private HGResult doUpDial()


    private void setReturnType() {

        setVariableDialAngleAndReturnDirection();

        if(angleSnapBaseOne != 0) {

            checkNextAngleSnapBaseOne();

        }

        if(angleSnapBaseOne != 0) {

            checkNextAngleSnapBaseOne();

        }

        if(suppressRender == false) {

            requestRender();

        }

    }//End private void setReturnType()
    /* Bottom of block rotate functions */


    /* Top of block main functions */
    private int setVariableDialAngleAndReturnDirection() {

        final int[] returnValue = new int[1];
        currentGestureAngleBaseOne = (1 - (onDownAngleObjectCumulativeBaseOne - touchAngle));
        final float angleDifference = (storedGestureAngleBaseOne - currentGestureAngleBaseOne) % 1;

        if(useVariablePages == true) {

            precisionRotation = getVariablePrecision();

        }
        else {

            precisionRotation = pagesPerDial;

        }

        //Detect direction
        if(!(Math.abs(angleDifference) > 0.75f)) {

            if(angleDifference > 0) {

                returnValue[0] = -1;
                fullGestureAngleBaseOne -= ((angleDifference * precisionRotation));

            }
            else if(angleDifference < 0) {

                returnValue[0] = 1;
                fullGestureAngleBaseOne += -(angleDifference * precisionRotation);

            }

        }

        storedGestureAngleBaseOne = currentGestureAngleBaseOne;

        if(fullGestureAngleBaseOne < 0f) {

            fullGestureAngleBaseOne = 0;
            hgglFastListInfo.setIsMinimumReached(true);

        }
        else if(fullGestureAngleBaseOne > maxRotation) {

            fullGestureAngleBaseOne = maxRotation;
            hgglFastListInfo.setIsMaximumReached(true);

        }
        else {

            hgglFastListInfo.setIsMinimumReached(false);
            hgglFastListInfo.setIsMaximumReached(false);

        }//End if(fullGestureAngleBaseOne < 0f)

        hgglFastListInfo.setScrollDirection(returnValue[0]);
        doDialScroll((Math.abs(angleDifference * recyclerViewHeight) * returnValue[0]));

        return returnValue[0];

    }//End private int setVariableDialAngleAndReturnDirection()


    public void setScrollableView(final View scrollableView) {

        scrollableView.post(new Runnable() {
            @Override
            public void run() {

                recyclerView = (RecyclerView) scrollableView;
                columnCount = recyclerView.getLayoutManager().getColumnCountForAccessibility(null, null);
                recyclerViewHeight = (float) recyclerView.getHeight();
                verticalRange = (float) recyclerView.computeVerticalScrollRange();
                itemCount = (float) recyclerView.getAdapter().getItemCount();
                itemsPerPage = itemCount / (verticalRange / recyclerViewHeight);
                maxRotation = (verticalRange / recyclerViewHeight) - (recyclerViewHeight / (verticalRange + recyclerViewHeight));

            }
        });

    }//End public void setScrollableView(final View scrollableView)


    private void doDialScroll(float scrollBy) {

        final int itemId = (int) (itemsPerPage * fullGestureAngleBaseOne);
        recyclerView.scrollBy(0, (int) (scrollBy * precisionRotation));
        hgglFastListInfo.setItemId(itemId);

        if(hgglFastListInfo.getItemId() != currentItemId) {

            hgglFastListInfo.setHasItemIdChanged(true);

        }
        else if(hgglFastListInfo.getItemId() == currentItemId) {

            hgglFastListInfo.setHasItemIdChanged(false);

        }//End if(hgFastListInfo.getItemId() != currentItemId)

        currentItemId = hgglFastListInfo.getItemId();

    }//End private void doDialScroll(float scrollBy)


    private float getVariablePrecision() {

        final float distanceFromCenter = HGGeometry.getTwoFingerDistance(viewCenterPoint.x, viewCenterPoint.y, touchXLocal, touchYLocal);

        if(distanceFromCenter <= viewPortMinimumDimensionOverTwo) {

            precisionRotation = (variableDialInner - (((distanceFromCenter / viewPortMinimumDimensionOverTwo) * (variableDialInner - variableDialOuter)) + variableDialOuter)) + variableDialOuter;

        }

        return precisionRotation;

    }//End private float getVariablePrecision()


    private void checkNextAngleSnapBaseOne() {

        final float tempAngleSnap = Math.round(fullGestureAngleBaseOne / angleSnapBaseOne);
        angleSnapNextBaseOne = tempAngleSnap * angleSnapBaseOne;

        if(Math.abs(fullGestureAngleBaseOne - angleSnapNextBaseOne) < angleSnapProximity) {

            angleSnapHasSnapped = true;
            hgglFastListInfo.setRotateSnapped(true);

        }
        else {

            angleSnapNextBaseOne = fullGestureAngleBaseOne;
            angleSnapHasSnapped = false;
            hgglFastListInfo.setRotateSnapped(false);

        }//End if(Math.abs(getAngleWrapper.getObjectAngleBaseOne() - angleSnapNextBaseOne) < angleSnapProximity)

    }//End private void checkNextAngleSnapBaseOne()
    /* Bottom of block main functions */


    /* Top of block Accessors */
    public long getQuickTapTime() {return this.quickTapTime;}
    public float getAngleSnapBaseOne() {return this.angleSnapBaseOne;}
    public float getAngleSnapProximity() {return this.angleSnapProximity;}
    public OnTouchListener retrieveLocalOnTouchListener() {return onTouchListener;}
    public float getDistortDialX() {return this.distortDialX;}
    public float getDistortDialY() {return this.distortDialY;}
    public boolean hasAngleSnapped() {return angleSnapHasSnapped;}
    public float getVariableDialInner() {return this.variableDialInner;}
    public float getVariableDialOuter() {return this.variableDialOuter;}
    public float getItemsPerPage() {return this.itemsPerPage;}
    public boolean getUseVariablePages() {return this.useVariablePages;}
    public boolean getSuppressRender() {return this.suppressRender;}
    public View getScrollableView() {return HGGLFastList.recyclerView;}
    public float getDialAngle() {return this.fullGestureAngleBaseOne;}
    public float getPrecisionRotation() {return this.precisionRotation;}


    private float getVariablePrecision(final Point touchPointLocal) {

        final float distanceFromCenter = HGGeometry.getTwoFingerDistance(viewCenterPoint.x, viewCenterPoint.y, touchPointLocal.x, touchPointLocal.y);
        final float returnValue = (((this.imageDiameter - Math.abs(distanceFromCenter)) / (float) viewCenterPoint.x) * ((variableDialInner - variableDialOuter)) + variableDialOuter);
        return returnValue;

    }//End private float getVariablePrecision(final Point touchPointLocal)
    /* Bottom of block Accessors */


    /* Top of block Mutators */
    public void setQuickTapTime(long quickTapTime) {this.quickTapTime = quickTapTime;}
    public void setUseVariablePages(boolean useVariablePages) {this.useVariablePages = useVariablePages;}
    public void setSuppressRender(final boolean suppressRender) {HGGLFastList.suppressRender = suppressRender;}


    public void setAngleSnapBaseOne(final float angleSnapBaseOne, final float angleSnapProximity) {

        this.angleSnapBaseOne = (angleSnapBaseOne) % 1;
        this.angleSnapProximity = (angleSnapProximity) % 1;
        if(angleSnapProximity > angleSnapBaseOne / 2f) {this.angleSnapProximity = angleSnapBaseOne / 2;}

    }//End public void setAngleSnapBaseOne(final float angleSnapBaseOne, final float angleSnapProximity)


    public void setDistortDialX(float distortDialX) {this.distortDialX = distortDialX;}
    public void setDistortDialY(float distortDialY) {this.distortDialY = distortDialY;}
    public void setDialAngle(float fullGestureAngleBaseOne) {this.fullGestureAngleBaseOne = fullGestureAngleBaseOne;}


    public void setVariableDial(float variableDialInner, float variableDialOuter, float pagesPerDial) {

        this.variableDialInner = variableDialInner;
        this.variableDialOuter = variableDialOuter;
        this.pagesPerDial = pagesPerDial;

    }//End public void setVariableDial(float variableDialInner, float variableDialOuter, float
    /* Bottom of block Mutators */


    @Override
    public void drawFrame(final Square square, final int activeTexture, final Point centerPoint, GL10 unused, final float[] rotationMatrix, final float[] matrixProjectionAndView) {

        float[] scratch = new float[16];
        Matrix.setIdentityM(rotationMatrix, 0);
        Matrix.translateM(rotationMatrix, 0, centerPoint.x + centreOffset.x, centerPoint.y + centreOffset.y, 0f);
        Matrix.scaleM(rotationMatrix, 0, distortDialX, distortDialY, 1f);

        if(angleSnapBaseOne == 0) {

            Matrix.rotateM(rotationMatrix, 0, - (fullGestureAngleBaseOne * 360f), 0.0f, 0.0f, 1.0f);

        }
        else if(angleSnapBaseOne != 0) {

            Matrix.rotateM(rotationMatrix, 0, - (angleSnapNextBaseOne * 360f), 0.0f, 0.0f, 1.0f);

        }//End if(rapidDial != 0)

        Matrix.translateM(rotationMatrix, 0, - centerPoint.x, - centerPoint.y, 0f);
        Matrix.multiplyMM(scratch, 0, matrixProjectionAndView, 0, rotationMatrix, 0);
        square.draw(scratch, activeTexture);

    }//End public void drawFrame(final Square square, final int activeTexture, final Point centerPoint, GL10 unused, final float[] rotationMatrix, final float[] matrixProjectionAndView)


    public static float[] getOriginalHeightAndWidth() {return new float[] {imageWidth, imageHeight};}


    public void performQuickTap() {

        hgglFastListInfo.setQuickTap(true);

        post(new Runnable() {
            @Override
            public void run() {

                iHGGLFastList.onUp(doUpDial());
                hgglFastListInfo.setQuickTap(false);

            }
        });

    }//End public void performQuickTap()


    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        this.ihgRender = null;
        this.imageResourceId = 0;
        this.onTouchListener = null;
        this.viewCenterPoint = null;
        this.iHGGLFastList = null;
        this.firstPointerId = 0;
        this.firstPointerIdx = 0;
        this.firstTouchX = 0f;
        this.firstTouchY = 0f;
        this.storedGestureAngleBaseOne = 0f;
        this.currentGestureAngleBaseOne = 0f;
        this.fullGestureAngleBaseOne = 0f;
        this.touchAngle = 0f;
        this.touchPointerCount = 0;
        this.onDownAngleObjectCumulativeBaseOne = 0f;
        this.onUpAngleGestureBaseOne = 0f;
        this.precisionRotation = 1f;
        this.angleSnapBaseOne = 0f;
        this.angleSnapProximity = 0f;
        this.angleSnapHasSnapped = false;
        this.touchXLocal = 0f;
        this.touchYLocal = 0f;
        this.quickTapTime = 100;
        this.gestureDownTime = 100;
        this.distortDialX = 1f;
        this.distortDialY = 1f;
        this.suppressRender = false;
        this.variableDialInner = 0f;
        this.variableDialOuter = 0f;
        this.imageDiameter = 0f;
        this.viewPortMinimumDimensionOverTwo = 0f;
        this.imageWidth = 0f;
        this.imageHeight = 0f;
        this.viewPortWidth = 0f;
        this.viewPortHeight = 0f;
        this.angleSnapNextBaseOne = 0f;
        this.recyclerViewHeight = 0f;
        this.verticalRange = 0f;
        this.pagesPerVerticalRange = 0f;
        this.itemCount = 0;
        this.pagesPerDial = 1f;
        this.hgglFastListInfo = null;

    }//End protected void onDetachedFromWindow()


    @Override
    public boolean onTouchEvent(MotionEvent event) {

        sendTouchEvent(this, event);

        return true;

    }


    private OnTouchListener getOnTouchListerField() {

        onTouchListener = new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                hgTouchEvent(v, event);

                return true;

            }

        };

        return onTouchListener;

    }//End private OnTouchListener getOnTouchListerField()


    public void sendTouchEvent(View v, MotionEvent event) {

        hgTouchEvent(v, event);

    }


    private void hgTouchEvent(View v, MotionEvent event) {

        final int action = event.getAction() & MotionEvent.ACTION_MASK;

        //Top of block used for quick tap
        if(event.getAction() == MotionEvent.ACTION_DOWN) {

            hgglFastListInfo.setQuickTap(false);//Used for quick tap
            gestureDownTime = System.currentTimeMillis();

        }
        else if(event.getAction() == MotionEvent.ACTION_UP) {

            //Used for quick tap
            if(System.currentTimeMillis() < gestureDownTime + quickTapTime) {

                hgglFastListInfo.setQuickTap(true);

            }
            else {

                hgglFastListInfo.setQuickTap(false);

            }//End if(System.currentTimeMillis() < gestureDownTime + quickTapTime)

        }//End if(event.getAction() == MotionEvent.ACTION_DOWN)
        //Bottom of block used for quick tap

        switch(action) {

            case MotionEvent.ACTION_DOWN: {

                setDownTouch(event);
                iHGGLFastList.onDown(doDownDial());

                break;

            }
            case MotionEvent.ACTION_POINTER_DOWN: {

                setDownTouch(event);
                iHGGLFastList.onDown(doDownDial());

                break;

            }
            case MotionEvent.ACTION_MOVE: {

                setMoveTouch(event);
                iHGGLFastList.onMove(doMoveDial());

                break;

            }
            case MotionEvent.ACTION_POINTER_UP: {

                setUpTouch(event);
                iHGGLFastList.onUp(doUpDial());

                break;

            }
            case MotionEvent.ACTION_UP: {

                setUpTouch(event);
                iHGGLFastList.onUp(doUpDial());

                break;

            }
            default:

                break;

        }//End switch(action)

    }//End private void hgTouchEvent(View v, MotionEvent event)

}