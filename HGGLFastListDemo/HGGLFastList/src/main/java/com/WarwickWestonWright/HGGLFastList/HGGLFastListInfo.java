/*
License. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2016, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.WarwickWestonWright.HGGLFastList;

final public class HGGLFastListInfo {

    private boolean quickTap;
    private boolean rotateSnapped;
    private boolean hasItemIdChanged;
    private boolean isMaximumReached;
    private boolean isMinimumReached;
    private int itemId;
    private int scrollDirection;

    public HGGLFastListInfo() {

        this.quickTap = false;
        this.rotateSnapped = false;
        this.hasItemIdChanged = false;
        this.isMaximumReached = false;
        this.isMinimumReached = false;
        this.itemId = -1;
        this.scrollDirection = 0;

    }//End public HGResult()

    /* Accessors */
    public boolean getQuickTap() {return this.quickTap;}
    public boolean getRotateSnapped() {return this.rotateSnapped;}
    public boolean getHasItemIdChanged() {return this.hasItemIdChanged;}
    public boolean getIsMaximumReached() {return this.isMaximumReached;}
    public boolean getIsMinimumReached() {return this.isMinimumReached;}
    public int getItemId() {return this.itemId;}
    public int getScrollDirection() {return this.scrollDirection;}

    /* Mutators */
    public void setQuickTap(boolean quickTap) {this.quickTap = quickTap;}
    public void setRotateSnapped(boolean rotateSnapped) {this.rotateSnapped = rotateSnapped;}
    public void setHasItemIdChanged(boolean hasItemIdChanged) {this.hasItemIdChanged = hasItemIdChanged;}
    public void setIsMaximumReached(boolean isMaximumReached) {this.isMaximumReached = isMaximumReached;}
    public void setIsMinimumReached(boolean isMinimumReached) {this.isMinimumReached = isMinimumReached;}
    public void setItemId(int itemId) {this.itemId = itemId;}
    public void setScrollDirection(int scrollDirection) {this.scrollDirection = scrollDirection;}

}