/*
License. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2016, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.WarwickWestonWright.HGGLFastList;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public final class HGGLRender implements GLSurfaceView.Renderer {

    public interface IHGRender {

        void drawFrame(Square square, int activeTexture, Point centerPoint, GL10 unused, float[] morphMatrix, float[] matrixProjectionAndView);

    }

    //HGRenderer Constants
    public static final int SCALE_TYPE_ORIGONAL_SIZE = 0;
    public static final int SCALE_TYPE_CENTER_CROP = 1;
    public static final int SCALE_TYPE_FIT_CENTER = 2;
    public static final int SCALE_TYPE_FIT_XY = 3;

    private static IHGRender ihgRender;
    private static float[] matrixProjection = new float[16];
    private static float[] matrixView = new float[16];
    private static float[] matrixProjectionAndView = new float[16];
    private static float[] morphMatrix = new float[16];
    private static int scaleType = SCALE_TYPE_FIT_CENTER;
    private static float scaleProportion = 0f;
    private static boolean relativeToWidth = true;
    private Square square = null;
    private Bitmap[] bitmaps = null;
    private static boolean useRapidDial = false;
    private Resources resources;
    private static int imageResourceId;
    private static int viewPortWidth;
    private static int viewPortHeight;
    private static int imageWidth;
    private static int imageHeight;
    private static Point viewCenterPoint;
    private static Point imageCenterPoint;

    public HGGLRender(Resources resources, int imageResourceId) {

        this.bitmaps = new Bitmap[1];
        this.square = null;
        this.resources = resources;
        this.imageResourceId = imageResourceId;
        this.viewPortWidth = 0;
        this.viewPortHeight = 0;
        this.imageWidth = 0;
        this.imageHeight = 0;
        this.viewCenterPoint = new Point(0, 0);
        this.imageCenterPoint = new Point(0, 0);

    }//End public HGRender(Resources resources, int imageResourceId)


    public static void registerRenderer(IHGRender ihgRender) {

        HGGLRender.ihgRender = ihgRender;

    }


    public static void setUseRapidDial(boolean useRapidDial) {
        HGGLRender.useRapidDial = useRapidDial;}
    public static boolean getUseRapidDial() {return HGGLRender.useRapidDial;}
    public static int[] getImageHeightAndWidth() {return new int[] {imageWidth, imageHeight};}
    public static int[] getViewPortHeightAndWidth() {return new int[] {viewPortWidth, viewPortHeight};}
    public static Point getViewCenterPoint() {return HGGLRender.viewCenterPoint;}
    public static Point getImageCenterPoint() {return HGGLRender.imageCenterPoint;}
    public static int getImageResourceId() {return imageResourceId;}


    /* Top of block scale type methods */
    public static void setHgScale(int scaleType) {

        HGGLRender.scaleType = scaleType;
        HGGLRender.scaleProportion = 0f;//Set to zero to be used as conditional flag

    }


    public static void setHgScale(float scaleProportion, final boolean relativeToWidth) {

        HGGLRender.scaleProportion = scaleProportion;
        HGGLRender.relativeToWidth = relativeToWidth;
        HGGLRender.scaleType = SCALE_TYPE_ORIGONAL_SIZE;//Set to zero to be used as conditional flag

    }
    /* Bottom of block scale type methods */


    @Override
    public void onSurfaceCreated(GL10 unused, EGLConfig config) {

        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glDisable(GLES20.GL_CULL_FACE);

    }//End public void onSurfaceCreated(GL10 unused, EGLConfig config)


    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height) {

        GLES20.glViewport(0, 0, width, height);
        viewPortWidth = width;
        viewPortHeight = height;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        options.inScreenDensity = (int) resources.getDisplayMetrics().density;
        options.inDither = true;
        options.inJustDecodeBounds = false;
        bitmaps[0] = BitmapFactory.decodeResource(resources, imageResourceId, options);

            /* Top of block scaling section */
        float imageX = (float) bitmaps[0].getWidth() / (float) options.inScreenDensity;
        float imageY = (float) bitmaps[0].getHeight() / (float) options.inScreenDensity;
        float imageProportion = imageX / imageY;
        float viewPortProportion = (float) width / (float) height;

        if(scaleProportion == 0f) {

            //Condition when setHgScale(int scaleType) called
            if(HGGLRender.scaleType != SCALE_TYPE_ORIGONAL_SIZE) {

                if(scaleType == SCALE_TYPE_FIT_CENTER) {

                    if(imageProportion >= viewPortProportion) {

                        bitmaps[0] = ImageUtilities.getProportionalBitmap(bitmaps[0], width, "X");

                    }
                    else if(imageProportion < viewPortProportion) {

                        bitmaps[0] = ImageUtilities.getProportionalBitmap(bitmaps[0], height, "Y");

                    }//End if(imageProportion >= viewPortProportion)

                }
                else if(scaleType == SCALE_TYPE_CENTER_CROP) {

                    if(imageX >= imageY) {

                        if(viewPortWidth < viewPortHeight) {

                            bitmaps[0] = ImageUtilities.getProportionalBitmap(bitmaps[0], height, "Y");

                        }
                        else if(viewPortWidth >= viewPortHeight) {

                            if(imageProportion >= viewPortProportion) {

                                bitmaps[0] = ImageUtilities.getProportionalBitmap(bitmaps[0], height, "Y");

                            }
                            else if(imageProportion < viewPortProportion) {

                                bitmaps[0] = ImageUtilities.getProportionalBitmap(bitmaps[0], width, "X");

                            }//End if(imageProportion >= viewPortProportion)

                        }//End if(viewPortWidth < viewPortHeight)

                    }
                    else if(imageX < imageY) {

                        if(viewPortWidth >= viewPortHeight) {

                            bitmaps[0] = ImageUtilities.getProportionalBitmap(bitmaps[0], width, "X");

                        }
                        else if(viewPortWidth < viewPortHeight) {

                            if(imageProportion >= viewPortProportion) {

                                bitmaps[0] = ImageUtilities.getProportionalBitmap(bitmaps[0], height, "Y");

                            }
                            else if(imageProportion < viewPortProportion) {

                                bitmaps[0] = ImageUtilities.getProportionalBitmap(bitmaps[0], width, "X");

                            }//End if(imageProportion >= viewPortProportion)

                        }//End if(viewPortWidth >= viewPortHeight)

                    }//End if(imageX >= imageY)

                }
                else if(scaleType == SCALE_TYPE_FIT_XY) {

                    bitmaps[0] = ImageUtilities.getScaledBitmap(bitmaps[0], width, height);

                }//End if(scaleType == SCALE_TYPE_FIT_CENTER)

            }
            //Condition when setHgScale(float scaleProportion, final boolean relativeToWidth) called
            else if(HGGLRender.scaleType == SCALE_TYPE_ORIGONAL_SIZE) {

                bitmaps[0] = ImageUtilities.getProportionalBitmap(bitmaps[0], bitmaps[0].getWidth() / options.inScreenDensity, "X");

            }//End if(HGRender.scaleType != SCALE_TYPE_ORIGONAL_SIZE)

        }
        //Condition for no scaling
        else if(scaleProportion != 0f) {

            if(relativeToWidth == true) {

                bitmaps[0] = ImageUtilities.getProportionalBitmap(bitmaps[0], (int) ((float) width / scaleProportion), "X");

            }
            else if(relativeToWidth == false) {

                bitmaps[0] = ImageUtilities.getProportionalBitmap(bitmaps[0], (int) ((float) height / scaleProportion), "Y");

            }//End if(relativeToWidth == true)

        }//End if(scaleProportion == 0f)
        /* Bottom of block scaling section */


        imageWidth = bitmaps[0].getWidth();
        imageHeight = bitmaps[0].getHeight();
        square = new Square(imageWidth, imageHeight, bitmaps);

        viewCenterPoint = new Point(width / 2, height / 2);
        imageCenterPoint = new Point((imageWidth / 2), imageHeight / 2);
        HGGLFastList.setupMetrics();

        for(int i = 0; i < 16; i++) {

            matrixProjection[i] = 0.0f;
            matrixView[i] = 0.0f;
            matrixProjectionAndView[i] = 0.0f;

        }

        Matrix.orthoM(matrixProjection, 0, 0f, width, 0.0f, height, -1f, 1f);
        Matrix.setLookAtM(matrixView, 0, 0f, 0f, 1f, 0f, 0f, 0f, 0f, 1.0f, 0f);
        Matrix.multiplyMM(matrixProjectionAndView, 0, matrixProjection, 0, matrixView, 0);

    }//End public void onSurfaceChanged(GL10 unused, int width, int height)


    @Override
    public void onDrawFrame(GL10 unused) {

        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        for(int i = 0; i < bitmaps.length; i++) {

            ihgRender.drawFrame(square, i, imageCenterPoint, unused, morphMatrix, matrixProjectionAndView);

        }

        //The call that sets useRapidDial to true is in the HGGLDial.doRapidDial method
        useRapidDial = false;

    }//End public void onDrawFrame(GL10 unused)

}