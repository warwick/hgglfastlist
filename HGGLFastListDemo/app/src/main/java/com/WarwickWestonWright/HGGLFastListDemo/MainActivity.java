/*
Though this is an open source repository, only the files contained within the library are protected under an open source license and not the files that use the library.
The code in this file is NOT protected by any license and is free source. Feel free to use modify and or distribute with no obligation to the developer. If you wish to contact the developer you can do so at: warwickwestonwright@gmail.com
*/

package com.WarwickWestonWright.HGGLFastListDemo;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.WarwickWestonWright.HGGLFastList.HGGLFastList;
import com.WarwickWestonWright.HGGLFastList.HGGLFastListInfo;
import com.WarwickWestonWright.HGGLFastListDemo.ListAdapters.MyItemRecyclerViewAdapter;
import com.WarwickWestonWright.HGGLFastListDemo.dummy.DummyContent;


public class MainActivity extends AppCompatActivity implements MainListFragment.IMainListFragment, DialogInterface.OnClickListener {

    private static MainListFragment mainListFragment;
    private SettingsFragment settingsFragment;
    private TextView lblStatus;
    private Button btnQuickSearch;
    private Button btnMultiSelect;
    private HGGLFastList hgglFastList;
    private static Point point;
    private static boolean multiSelect;
    private static int releaseCount;
    private static int[] currentSelectionRange;
    private AlertDialog.Builder alertDialogBuilder;
    private static RecyclerView recyclerView;
    private static HGGLFastListInfo hgglFastListInfo;
    private static RecyclerView.OnScrollListener onScrollListener;
    private static RecyclerView.OnChildAttachStateChangeListener onChildAttachStateChangeListener;
    private static SharedPreferences sharedPreferences;
    private static boolean selectionComplete;
    private static int itemCount;
    private static Bundle savedInstanceState;
    private static boolean maximumScrollReached;
    private static boolean minimumScrollReached;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.savedInstanceState = savedInstanceState;
        setContentView(R.layout.main_activity);

        multiSelect = false;
        releaseCount = 0;
        currentSelectionRange = new int[2];
        currentSelectionRange[0] = -1;
        currentSelectionRange[1] = -1;
        selectionComplete = false;
        itemCount = 0;
        maximumScrollReached = false;
        minimumScrollReached = false;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        point = new Point(getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels);
        lblStatus = (TextView) findViewById(R.id.lblStatus);
        btnQuickSearch = (Button) findViewById(R.id.btnQuickSearch);
        btnMultiSelect = (Button) findViewById(R.id.btnMultiSelect);
        hgglFastList = (HGGLFastList) findViewById(R.id.hgglFastListView);
        hgglFastList.setVisibility(View.GONE);

        if(point.x < point.y) {

            hgglFastList.getLayoutParams().width = point.x;
            hgglFastList.getLayoutParams().height = point.x;

        }
        else if(point.x >= point.y) {

            hgglFastList.getLayoutParams().width = point.y;
            hgglFastList.getLayoutParams().height = point.y;

        }//End if(point.x < point.y)

        hgglFastListInfo = hgglFastList.getHgGlFastList();
        alertDialogBuilder = new AlertDialog.Builder(this);

        if(sharedPreferences.getBoolean("spIsSetup", false) == false) {

            hgglFastList.setVariableDial(4.5f, 0.8f, 8f);

            //Initialise Rotate settings.
            sharedPreferences.edit().putFloat("txtInnerPrecision", 4.5f).commit();
            sharedPreferences.edit().putFloat("txtOuterPrecision", 0.8f).commit();
            sharedPreferences.edit().putFloat("txtAngleSnap", 0.125f).commit();
            sharedPreferences.edit().putFloat("txtAngleSnapProximity", 0.03125f).commit();
            sharedPreferences.edit().putBoolean("chkUseAngleSnap", false).commit();
            sharedPreferences.edit().putBoolean("chkUseVariablePages", true).commit();
            sharedPreferences.edit().putFloat("txtPagesPerDial", 8f).commit();
            sharedPreferences.edit().putBoolean("chkSuppressRender", false).commit();
            sharedPreferences.edit().putInt("txtListLength", 1000).commit();
            DummyContent.setCount(sharedPreferences.getInt("txtListLength", 1000));

            sharedPreferences.edit().putBoolean("spIsSetup", true).commit();

        }
        else if(sharedPreferences.getBoolean("spIsSetup", false) == true) {

            hgglFastList.setVariableDial(sharedPreferences.getFloat("txtInnerPrecision", 4.5f), sharedPreferences.getFloat("txtOuterPrecision", 0.8f), sharedPreferences.getFloat("txtPagesPerDial", 8f));

            if(sharedPreferences.getBoolean("chkUseAngleSnap", false) == true) {

                hgglFastList.setAngleSnapBaseOne(sharedPreferences.getFloat("txtAngleSnap", 0.125f), sharedPreferences.getFloat("txtAngleSnapProximity", 0.03125f));

            }
            else if(sharedPreferences.getBoolean("chkUseAngleSnap", false) == false) {

                hgglFastList.setAngleSnapBaseOne(0f, 0f);

            }//End if(sharedPreferences.getBoolean("", false) == true)

            hgglFastList.setUseVariablePages(sharedPreferences.getBoolean("chkUseVariablePages", true));
            hgglFastList.setSuppressRender(sharedPreferences.getBoolean("chkSuppressRender", false));
            DummyContent.setCount(sharedPreferences.getInt("txtListLength", 1000));

        }//End if(sharedPreferences.getBoolean("spIsSetup", false) == false)

        btnQuickSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                hgglFastList.setDialAngle(0f);

                if(point.x < point.y) {

                    hgglFastList.getLayoutParams().width = point.x;
                    hgglFastList.getLayoutParams().height = point.x;

                }
                else if(point.x >= point.y) {

                    hgglFastList.getLayoutParams().width = recyclerView.getHeight();
                    hgglFastList.getLayoutParams().height = recyclerView.getHeight();

                }//End if(point.x < point.y)

                currentSelectionRange[0] = 0; currentSelectionRange[1] = 0;
                recyclerView.setAdapter(recyclerView.getAdapter());
                selectionComplete = false;
                alertDialogBuilder.setTitle("Single Selection").setMessage("Rotate and Release to Select").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        hgglFastList.setVisibility(View.VISIBLE);

                    }
                }).show();

                multiSelect = false;
                releaseCount = 0;

            }
        });

        btnMultiSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                hgglFastList.setDialAngle(0f);

                if(point.x < point.y) {

                    hgglFastList.getLayoutParams().width = point.x;
                    hgglFastList.getLayoutParams().height = point.x;

                }
                else if(point.x >= point.y) {

                    hgglFastList.getLayoutParams().width = recyclerView.getHeight();
                    hgglFastList.getLayoutParams().height = recyclerView.getHeight();

                }//End if(point.x < point.y)

                currentSelectionRange[0] = 0; currentSelectionRange[1] = 0;
                recyclerView.setAdapter(recyclerView.getAdapter());
                selectionComplete = false;
                alertDialogBuilder.setTitle("Multi Selection").setMessage("Rotate and Release to Select First Item, then repeat for second item").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        hgglFastList.setVisibility(View.VISIBLE);

                    }
                }).show();

                multiSelect = true;
                releaseCount = 0;

            }
        });

        hgglFastList.registerCallback(new HGGLFastList.IHGGLFastList() {
            @Override
            public void onDown(HGGLFastListInfo iHGGLFastList) {MainActivity.hgglFastListInfo = iHGGLFastList;}
            @Override
            public void onPointerDown(HGGLFastListInfo iHGGLFastList) {}

            @Override
            public void onMove(HGGLFastListInfo iHGGLFastList) {

                lblStatus.setText("Current Item: " + Integer.toString(iHGGLFastList.getItemId()));

                if(iHGGLFastList.getHasItemIdChanged()) {

                    if(multiSelect == false && selectionComplete == false) {

                        resetViewStylesSingleSelect(iHGGLFastList.getItemId());

                    }
                    else if(multiSelect == true && selectionComplete == false && releaseCount == 0) {

                        resetViewStylesSingleSelect(iHGGLFastList.getItemId());

                    }
                    else if(multiSelect == true && selectionComplete == false && releaseCount == 1) {

                        setSelectionRange();
                        resetViewStylesMultiSelect(iHGGLFastList.getItemId());

                    }

                }

            }

            @Override
            public void onPointerUp(HGGLFastListInfo iHGGLFastList) {}

            @Override
            public void onUp(HGGLFastListInfo hgglFastList) {

                if(multiSelect == false) {

                    currentSelectionRange[releaseCount] = hgglFastList.getItemId();

                    alertDialogBuilder.setTitle("Your selection was: ").setMessage(Integer.toString(currentSelectionRange[0])).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            releaseCount = 0;
                            MainActivity.this.hgglFastList.setVisibility(View.GONE);
                            selectionComplete = true;
                            onScrollListener.onScrollStateChanged(recyclerView, RecyclerView.SCROLL_STATE_IDLE);

                        }
                    }).show();

                }
                else if(multiSelect == true) {

                    currentSelectionRange[releaseCount] = hgglFastList.getItemId();
                    releaseCount++;

                    if(releaseCount == 2) {

                        //Swap first selection if necessary
                        if(currentSelectionRange[0] > currentSelectionRange[1]) {

                            int temp = currentSelectionRange[0];
                            currentSelectionRange[0] = currentSelectionRange[1];
                            currentSelectionRange[1] = temp;

                        }

                        alertDialogBuilder.setTitle("Your selection range was: ").setMessage(Integer.toString(currentSelectionRange[0]) + " to " + Integer.toString(currentSelectionRange[1])).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                releaseCount = 0;
                                MainActivity.this.hgglFastList.setVisibility(View.GONE);
                                selectionComplete = true;
                                onScrollListener.onScrollStateChanged(recyclerView, RecyclerView.SCROLL_STATE_IDLE);

                            }
                        }).show();

                    }
                    else if(releaseCount == 1) {

                        Toast.makeText(getBaseContext(), "Now Make Your Second Selection!", Toast.LENGTH_LONG).show();

                    }//End if(releaseCount == 2)

                }//End if(multiSelect == false && releaseCount == 0)

            }

        });

        if(savedInstanceState == null) {

            mainListFragment = new MainListFragment().newInstance(1);
            getSupportFragmentManager().beginTransaction().replace(R.id.rLayoutListContainer, mainListFragment, "MainListFragment").commit();

        }

        /* Top of block attache list listeners*/
        onScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }
        };

        onChildAttachStateChangeListener = new RecyclerView.OnChildAttachStateChangeListener() {
            @Override
            public void onChildViewAttachedToWindow(View view) {

                if(multiSelect == false && selectionComplete == true) {

                    ((CardView) ((RelativeLayout) view).getChildAt(0)).getChildAt(0).setBackgroundColor(Color.parseColor("#E040FB"));

                    try {

                        ((CardView) ((RelativeLayout) recyclerView.findViewHolderForAdapterPosition(hgglFastListInfo.getItemId()).itemView).getChildAt(0)).getChildAt(0).setBackgroundColor(Color.parseColor("#FFFFFF"));

                    }
                    catch(NullPointerException e) {}

                }
                else if(multiSelect == true && selectionComplete == true) {

                    final int currentId = Integer.parseInt(((TextView) ((LinearLayout) ((CardView) ((RelativeLayout) view).getChildAt(0)).getChildAt(0)).getChildAt(0)).getText().toString());
                    resetViewStylesMultiSelect(currentId);

                }

            }
            @Override
            public void onChildViewDetachedFromWindow(View view) {}
        };
        /* Bottom of block attache list listeners*/

    }//End protected void onCreate(Bundle savedInstanceState)


    private void resetViewStylesMultiSelect(final int currentId) {

        if(currentSelectionRange[0] != currentSelectionRange[1]) {

            try {

                for(int i = currentId; i > -1; i--) {

                    if(isInSelectedRange(i) == true) {

                        ((CardView) ((RelativeLayout) recyclerView.findViewHolderForAdapterPosition(i).itemView).getChildAt(0)).getChildAt(0).setBackgroundColor(Color.parseColor("#FFFFFF"));

                    }
                    else {

                        ((CardView) ((RelativeLayout) recyclerView.findViewHolderForAdapterPosition(i).itemView).getChildAt(0)).getChildAt(0).setBackgroundColor(Color.parseColor("#E040FB"));

                    }//End if(isInSelectedRange(currentId) == true)

                }

            }
            catch(NullPointerException e) {}

            try {

                for(int i = currentId; i < itemCount; i++) {

                    if(isInSelectedRange(i) == true) {

                        ((CardView) ((RelativeLayout) recyclerView.findViewHolderForAdapterPosition(i).itemView).getChildAt(0)).getChildAt(0).setBackgroundColor(Color.parseColor("#FFFFFF"));

                    }
                    else {

                        ((CardView) ((RelativeLayout) recyclerView.findViewHolderForAdapterPosition(i).itemView).getChildAt(0)).getChildAt(0).setBackgroundColor(Color.parseColor("#E040FB"));

                    }//End if(isInSelectedRange(currentId) == true)

                }

            }
            catch(NullPointerException e) {}

        }
        else if(currentSelectionRange[0] == currentSelectionRange[1]) {

            try {

                ((CardView) ((RelativeLayout) recyclerView.findViewHolderForAdapterPosition(currentId - 1).itemView).getChildAt(0)).getChildAt(0).setBackgroundColor(Color.parseColor("#E040FB"));
                ((CardView) ((RelativeLayout) recyclerView.findViewHolderForAdapterPosition(currentId).itemView).getChildAt(0)).getChildAt(0).setBackgroundColor(Color.parseColor("#FFFFFF"));
                ((CardView) ((RelativeLayout) recyclerView.findViewHolderForAdapterPosition(currentId + 1).itemView).getChildAt(0)).getChildAt(0).setBackgroundColor(Color.parseColor("#E040FB"));

            }
            catch(NullPointerException e) {}

        }//End if(currentSelectionRange[0] != currentSelectionRange[1])

    }//End private void resetViewStylesMultiSelect(final int currentId)


    private void resetViewStylesSingleSelect(final int currentId) {

        try {

            for(int i = currentId - 1; i > 0; i--) {

                ((CardView) ((RelativeLayout) recyclerView.findViewHolderForAdapterPosition(i).itemView).getChildAt(0)).getChildAt(0).setBackgroundColor(Color.parseColor("#E040FB"));

            }

        }
        catch(NullPointerException e) {}

        try {

            for(int i = currentId + 1; i < itemCount; i++) {

                ((CardView) ((RelativeLayout) recyclerView.findViewHolderForAdapterPosition(i).itemView).getChildAt(0)).getChildAt(0).setBackgroundColor(Color.parseColor("#E040FB"));

            }

        }
        catch(NullPointerException e) {}

        try {

            ((CardView) ((RelativeLayout) recyclerView.findViewHolderForAdapterPosition(currentId).itemView).getChildAt(0)).getChildAt(0).setBackgroundColor(Color.parseColor("#FFFFFF"));

        }
        catch(NullPointerException e) {}

    }//End private void resetViewStylesSingleSelect(final int currentId)


    private void setSelectionRange() {

        currentSelectionRange[1] = hgglFastListInfo.getItemId();

    }//End private void setSelectionRange()


    private boolean isInSelectedRange(final int currentId) {

        if(currentSelectionRange[1] < currentSelectionRange[0]) {

            if(currentId <= currentSelectionRange[0] && currentId >= currentSelectionRange[1]) {

                return true;

            }

        }
        else if(currentSelectionRange[1] > currentSelectionRange[0]) {

            if(currentId >= currentSelectionRange[0] && currentId <= currentSelectionRange[1]) {

                return true;

            }

        }
        else if(currentSelectionRange[1] == currentSelectionRange[0]) {

            return true;

        }//End if(currentSelectionRange[1] < currentSelectionRange[0])

        return false;

    }//End private boolean isInSelectedRange(final int currentId)


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);

        return true;

    }//End public boolean onCreateOptionsMenu(Menu menu)


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //Handle item selection
        switch (item.getItemId()) {
            case R.id.action_settings:

                settingsFragment = new SettingsFragment();
                settingsFragment.setTargetFragment(settingsFragment, 0);
                settingsFragment.setCancelable(false);
                settingsFragment.show(getSupportFragmentManager(), "SettingsFragment");

                return true;

            default:

                return super.onOptionsItemSelected(item);

        }

    }//End public boolean onOptionsItemSelected(MenuItem item)


    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onResume() {
        super.onResume();

        if(this.savedInstanceState != null) {

            if(point.x < point.y) {

                mainListFragment = new MainListFragment().newInstance(1);
                getSupportFragmentManager().beginTransaction().replace(R.id.rLayoutListContainer, mainListFragment, "MainListFragment").commit();

            }
            else if(point.x >= point.y) {

                mainListFragment = new MainListFragment().newInstance(2);
                getSupportFragmentManager().beginTransaction().replace(R.id.rLayoutListContainer, mainListFragment, "MainListFragment").commit();

            }//End if(point.x < point.y)

        }//End if(this.savedInstanceState != null)

    }//End public void onResume()


    @Override
    public void mainFragmentListener(DummyContent.DummyItem item) {}


    @Override
    public void sendInflatedListView(RecyclerView recyclerView) {

        MainActivity.recyclerView = mainListFragment.getScrollableView();
        hgglFastList.setScrollableView(recyclerView);
        MainActivity.recyclerView.addOnScrollListener(onScrollListener);
        MainActivity.recyclerView.addOnChildAttachStateChangeListener(onChildAttachStateChangeListener);
        itemCount = recyclerView.getAdapter().getItemCount();

    }//End public void sendInflatedListView(RecyclerView recyclerView)


    @Override
    public void onClick(DialogInterface dialogInterface, int i) {

        dialogInterface.cancel();
        hgglFastList.setVariableDial(sharedPreferences.getFloat("txtInnerPrecision", 4.5f), sharedPreferences.getFloat("txtOuterPrecision", 0.8f), sharedPreferences.getFloat("txtPagesPerDial", 8f));
        hgglFastList.setUseVariablePages(sharedPreferences.getBoolean("chkUseVariablePages", true));

        if(sharedPreferences.getBoolean("chkUseAngleSnap", false) == true) {

            hgglFastList.setAngleSnapBaseOne(sharedPreferences.getFloat("txtAngleSnap", 0.125f), sharedPreferences.getFloat("txtAngleSnapProximity", 0.03125f));

        }
        else if(sharedPreferences.getBoolean("chkUseAngleSnap", false) == false) {

            hgglFastList.setAngleSnapBaseOne(0f, 0f);

        }//End if(sharedPreferences.getBoolean("", false) == true)

        hgglFastList.setSuppressRender(sharedPreferences.getBoolean("chkSuppressRender", false));

        if(i != 0) {

            DummyContent.setCount(i);
            MainListFragment.getRecyclerView().setAdapter(new MyItemRecyclerViewAdapter(DummyContent.ITEMS, null));
            hgglFastList.setScrollableView(mainListFragment.getScrollableView());
            getSupportFragmentManager().beginTransaction().replace(R.id.rLayoutListContainer, mainListFragment, "MainListFragment").commit();

        }

    }//End public void onClick(DialogInterface dialogInterface, int i)

}