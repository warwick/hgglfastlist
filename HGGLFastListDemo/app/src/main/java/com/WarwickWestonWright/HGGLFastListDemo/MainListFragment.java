/*
Though this is an open source repository, only the files contained within the library are protected under an open source license and not the files that use the library.
The code in this file is NOT protected by any license and is free source. Feel free to use modify and or distribute with no obligation to the developer. If you wish to contact the developer you can do so at: warwickwestonwright@gmail.com
*/

package com.WarwickWestonWright.HGGLFastListDemo;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.WarwickWestonWright.HGGLFastListDemo.ListAdapters.MyItemRecyclerViewAdapter;
import com.WarwickWestonWright.HGGLFastListDemo.dummy.DummyContent;

public class MainListFragment extends Fragment {

    public interface IMainListFragment {

        void mainFragmentListener(DummyContent.DummyItem item);
        void sendInflatedListView(RecyclerView recyclerView);

    }

    private View rootView;
    private static final String ARG_COLUMN_COUNT = "column-count";
    private int columnCount = 2;
    private IMainListFragment iMainListFragment;
    private static RecyclerView recyclerView;

    public MainListFragment() {}

    @SuppressWarnings("unused")
    public static MainListFragment newInstance(int columnCount) {

        MainListFragment fragment = new MainListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);

        return fragment;

    }


    public RecyclerView getScrollableView() {return recyclerView;}


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments() != null) {

            columnCount = getArguments().getInt(ARG_COLUMN_COUNT);

        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.main_fragment_list_layout, container, false);

        if(rootView instanceof RecyclerView) {

            Context context = rootView.getContext();
            recyclerView = (RecyclerView) rootView;

            if(columnCount <= 1) {

                //recyclerView.setLayoutManager(new HGGLSmoothScroller(getContext(), LinearLayoutManager.VERTICAL, false));
                recyclerView.setLayoutManager(new LinearLayoutManager(context));

            }
            else {

                recyclerView.setLayoutManager(new GridLayoutManager(context, columnCount));

            }

            recyclerView.setAdapter(new MyItemRecyclerViewAdapter(DummyContent.ITEMS, iMainListFragment));

        }

        iMainListFragment.sendInflatedListView(recyclerView);

        return rootView;

    }//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


    public static RecyclerView getRecyclerView() {

        return MainListFragment.recyclerView;

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context instanceof IMainListFragment) {

            iMainListFragment = (IMainListFragment) context;

        }
        else {

            throw new RuntimeException(context.toString() + " must implement IMainListFragment");

        }

    }


    @Override
    public void onDetach() {
        super.onDetach();

        iMainListFragment = null;

    }

}